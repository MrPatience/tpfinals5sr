#!/bin/bash
user=$1
#On copie les fichiers nécessaires pour le serveur et pour l'utilisateur
#Création dossier Exo1
mkdir /Exo1
#Appartenance du dossier
chown -R $user:$user /Exo1
chown -R $user:$user /Exo1/*
#Copie de la chaine de confiance
cp /root/ca/intermediate/certs/ca-chain.cert.pem /Exo1/
#Copie de la clé privée du serveur web, ainsi que son Certificat
cp /root/ca/intermediate/private/webserver.key /Exo1/
cp /root/ca/intermediate/certs/webserver.crt /Exo1/
#Copie du certificat du CA Root pour le récupérer sur les utilisateurs
cp /root/ca/certs/ca.cert.pem /home/$user/CARoot.pem
cp /home/$user/CARoot.pem /var/www/html/
#Copie d'un virtualhost HTTPS fonctionnel et configuré, pour apache2
cp /home/$user/Exo1/config/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf
