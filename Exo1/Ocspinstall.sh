#!/bin/bash
cd /root/ca
#On génère la clef pour ocsp
openssl genrsa -aes256 \
      -out intermediate/private/ocsp.key.pem 4096
#on génère sa CSR à faire signer par intermediate
openssl req -config intermediate/openssl.cnf -new -sha256 \
      -key intermediate/private/ocsp.key.pem \
      -out intermediate/csr/ocsp.csr.pem
#On signe le certificat
openssl ca -config intermediate/openssl.cnf \
      -extensions ocsp -days 375 -notext -md sha256 \
      -in intermediate/csr/ocsp.csr.pem \
      -out intermediate/certs/ocsp.cert.pem
#On la vérifie
openssl x509 -noout -text \
      -in intermediate/certs/ocsp.cert.pem
