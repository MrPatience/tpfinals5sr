#!/bin/bash
#creation clef pour serveur
cd /root/ca
openssl genrsa -aes256 \
      -out intermediate/private/webserver.key 2048
#droits du fichier crée
chmod 400 intermediate/private/webserver.key
#Creation certificat webserver
cd /root/ca
openssl req -config intermediate/openssl.cnf \
      -key intermediate/private/webserver.key \
      -new -sha256 -out intermediate/csr/webserver.csr
#Signature par le CA intermédiaire
cd /root/ca
openssl ca -config intermediate/openssl.cnf \
      -extensions server_cert -days 375 -notext -md sha256 \
      -in intermediate/csr/webserver.csr \
      -out intermediate/certs/webserver.crt
#droits
chmod 444 intermediate/certs/webserver.crt
#Verification certificat
openssl x509 -noout -text \
      -in intermediate/certs/webserver.crt
#on vérifie que la chaine de parenté est toujours valide
openssl verify -CAfile intermediate/certs/ca-chain.cert.pem \
      intermediate/certs/webserver.crt
sleep 3
