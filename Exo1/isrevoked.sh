#!/bin/bash
cd /root/ca/

openssl ocsp -CAfile intermediate/certs/ca-chain.cert.pem \
      -url http://10.10.10.1:2560 -resp_text \
      -issuer intermediate/certs/intermediate.cert.pem \
      -cert intermediate/certs/webserver.crt
