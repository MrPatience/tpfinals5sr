#!/bin/bash
user=$1
#Creation des dossiers de travail
mkdir /root/ca
#Creation structure du dossier - Les fichiers index et serial servent juste de stockage de données pour tracer les certificats signésh
cd /root/ca
mkdir certs crl newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial
#On copie le fichier de config importé openssl pour CARoot vers son dossier
cp /home/$user/Exo1/root/openssl.cnf /root/ca/openssl.cnf
#Creation de la clef privée root avec AES256 + mot de passe fort
openssl genrsa -aes256 -out private/ca.key.pem 4096
#On donne les droits à l'utilisateur qui a crée la clef uniquement. 400 = read-only,none,none
chmod 400 private/ca.key.pem
#Creation du certificat root
openssl req -config openssl.cnf \
      -key private/ca.key.pem \
      -new -x509 -days 7300 -sha256 -extensions v3_ca \
      -out certs/ca.cert.pem
#On donne les droits au fichier pour tout le monde en read-only. 444 = read-only,read-only,read-only.
chmod 444 certs/ca.cert.pem
#Verification du certificat
openssl x509 -noout -text -in certs/ca.cert.pem
