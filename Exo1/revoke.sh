#!/bin/bash
user=$1
cd /root/ca
#On révoque le certificat du serveur web.
openssl ca -config intermediate/openssl.cnf \
            -revoke intermediate/certs/webserver.crt
echo "redémarrez le serveur ocsp pour mettre à jour la liste des certificats révoqués"
sleep 5
