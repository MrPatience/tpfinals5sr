#!/bin/bash
user=$1
#On prépare le dossier pour le CA intermédiaire
mkdir /root/ca/intermediate
#on recrée la même structure que pour CA Root
cd /root/ca/intermediate
mkdir certs crl csr newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial
#On ajoute un crlnumber, il sert à conserver la trace des CRL
echo 1000 > /root/ca/intermediate/crlnumber
#On copie la configuration, qui est quasi identique que pour le CA Root, mais un peu plus permissive
cp /home/$user/Exo1/intermediate/openssl.cnf /root/ca/intermediate/openssl.cnf
#On crée la clef privée de l'intermediate
cd /root/ca
openssl genrsa -aes256 -out intermediate/private/intermediate.key.pem 4096
#Attribution des droits
chmod 400 intermediate/private/intermediate.key.pem
#Creation du certificat
cd /root/ca
openssl req -config intermediate/openssl.cnf -new -sha256 \
      -key intermediate/private/intermediate.key.pem \
      -out intermediate/csr/intermediate.csr.pem
#signé avec config pour qu'il soit bien CA intermediaire
cd /root/ca
openssl ca -config openssl.cnf -extensions v3_intermediate_ca \
      -days 3650 -notext -md sha256 \
      -in intermediate/csr/intermediate.csr.pem \
      -out intermediate/certs/intermediate.cert.pem
#droits
chmod 444 intermediate/certs/intermediate.cert.pem
#Verification certificat
openssl x509 -noout -text \
      -in intermediate/certs/intermediate.cert.pem
#Verification chaine root > intermédiaire est OK
openssl verify -CAfile certs/ca.cert.pem \
      intermediate/certs/intermediate.cert.pem
#Créer le fichier chaîne de certificat | Tous les certificats les uns après les autres
cat intermediate/certs/intermediate.cert.pem \
      certs/ca.cert.pem > intermediate/certs/ca-chain.cert.pem
#droits du fichier crée
chmod 444 intermediate/certs/ca-chain.cert.pem
