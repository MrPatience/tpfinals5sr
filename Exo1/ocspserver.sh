#!/bin/bash
cd /root/ca
openssl ocsp -port 2560 -text \
                  -index intermediate/index.txt \
                  -CA intermediate/certs/ca-chain.cert.pem \
                  -rkey intermediate/private/ocsp.key.pem \
                  -rsigner intermediate/certs/ocsp.cert.pem \
