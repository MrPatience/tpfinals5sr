#!/bin/bash
# Detect Debian users running the script with "sh" instead of bash
if readlink /proc/$$/exe | grep -q "dash"; then
	echo "This script needs to be run with bash, not sh"
	exit
fi
if [[ "$EUID" -ne 0 ]]; then
	echo "Sorry, you need to run this as root"
	exit
fi
##Procedure Exo 1 TP
#Declaraiton de variables
read  -n 254 -p "Nom de l'utilisateur système:" user
#Installation serveur WEB Apache
bash apache2.sh
#Mise en place Certificat Root
bash CARoot.sh $user
#Mise en place Certificat Intermédiaire & Signature par Root
bash CAInt.sh $user
#Mise en place certificat serveur & signature par intermédiaire
bash CAWebserver.sh $user
#Copie des fichiers pour mettre en place le serveur HTTPS + certificat pour client web
bash Copy.sh $user
#Activation du SSL pour Apache2, du virtualhost en HTTPS et redémarrage d'Apache2.
a2enmod ssl
a2ensite default-ssl
service apache2 restart
#Installation OCSP
bash Ocspinstall.sh $user
