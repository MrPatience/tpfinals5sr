#!/usr/bin/env bash

#On génère ma clef privée personnelle
echo "On va générer notre propre clef"
gpg --full-generate-key
#On copie l'ID pour générer la clef publique
echo "On doit désormais générer la clef publique en tant que maclef.asc"
read  -n 254 -p "En attente de l'ID de la clef :" keygpg
gpg --output maclef.asc --armor --export $keygpg
ls && sleep 10

#On importe la clef publique dans gpg
echo "On importe la clef publique de l'ami dans gpg avec gpg --import"
echo "gpg --import saclef.asc"
sleep 3

#On signe un message avec ma clef privée
echo "On signe un message \" Bonjour à tous!\" avec notre clef privée et la commande gpg --clearsign"
echo "Bonjour à tous!" > message2
gpg --clearsign message2
sleep 3
#On vérifie la signature en utilisant la clef publique
echo "On vérifie la signature avec la clef publique et la commande gpg --verify"
gpg --verify message2.asc
sleep 5
#On crée un message et on le chiffre"
echo "On crée un message et on le chiffre avec notre clef publique, comme si l'ami le chiffrait, avec la commande gpg --recipient alexandre --encrypt message"
echo "Hello World! :)" > message
gpg --recipient alexandre --encrypt message
sleep 5
#On le déchiffre avec notre clef privée
echo "On le déchiffre avec notre clef privée et la commande gpg --decrypt"
gpg --decrypt message.gpg
