#!/usr/bin/env bash
read  -n 254 -p "Nom de l'utilisateur système:" user
sudo apt-get update && sudo apt-get install openvpn
sudo cp -a /usr/share/easy-rsa /etc/openvpn/
cd /etc/openvpn/easy-rsa
sudo source vars
sudo ./clean-all

cd /etc/openvpn/easy-rsa
sudo ln -s openssl-1.0.0.cnf openssl.cnf
sudo ./build-ca
sudo ./build-dh
sudo ./build-key-server srvcert
sudo gunzip -c /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz > /etc/openvpn/server.conf
sudo vim /etc/openvpn/server.conf
sudo service openvpn stop
sudo openvpn /etc/openvpn/server.conf
sleep 3
ifconfig tun0
sleep 5
sudo service openvpn start
sudo echo "net.ipv4.ip_forward=1" > /etc/sysctl.d/NAT.conf
sudo sysctl -p /etc/sysctl.d/NAT.conf
iptables -t filter -P FORWARD ACCEPT
iptables -t filter -A INPUT -p udp --dport 1194 -j ACCEPT
iptables -t nat -A POSTROUTING -o ethx(nom de votre interface)
iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o ethx(nom de votre interface) -j MASQUERADE
