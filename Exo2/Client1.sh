#!/usr/bin/env bash
if [[ "$EUID" -ne 0 ]]; then
	echo "Sorry, you need to run this as root"
	exit
fi

apt update && sudo apt upgrade -y
apt install strongswan -y

cat >> /etc/ipsec.secrets << EOF
# source      destination
10.10.20.2 10.10.20.1 : PSK "R0ut3r!Cl13nt"
EOF

cat >> /etc/ipsec.conf << EOF
# connection client1-to-router1
conn client1-to-router1
    authby=secret
    auto=route
    keyexchange=ike
    left=10.10.20.2
    right=10.10.20.1
    type=transport
    esp=aes128gcm16!

EOF
iptables -t nat -A POSTROUTING -s 10.10.20.0/24 -d 10.10.30.0/24

ipsec restart
