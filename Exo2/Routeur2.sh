#!/usr/bin/env bash
if [[ "$EUID" -ne 0 ]]; then
	echo "Sorry, you need to run this as root"
	exit
fi
apt update && sudo apt upgrade -y
apt install strongswan -y

cat >> /etc/sysctl.conf << EOF
net.ipv4.ip_forward = 1
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
EOF

sysctl -p /etc/sysctl.conf

read  -n 254 -p "En attente de la PSK :" PSK

cat >> /etc/ipsec.secrets << EOF
# source      destination
10.10.10.2 10.10.10.1 : PSK "$PSK"

EOF

cat >> /etc/ipsec.conf << EOF
# basic configuration
config setup
        charondebug="all"
        uniqueids=yes
        strictcrlpolicy=no

# connection to site 1
conn site2-to-site1
  authby=secret
  left=%defaultroute
  leftid=10.10.10.2
  leftsubnet=10.10.30.1/24
  right=10.10.10.1
  rightsubnet=10.10.20.1/24
  ike=aes256-sha2_256-modp1024!
  esp=aes256-sha2_256!
  keyingtries=0
  ikelifetime=1h
  lifetime=8h
  dpddelay=30
  dpdtimeout=120
  dpdaction=restart
  auto=start
EOF
iptables -t nat -A POSTROUTING -s 10.10.30.0/24 -d 10.10.20.0/24
ipsec restart
